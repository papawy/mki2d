#pragma once

#include <SFML\System.hpp>

#include <Box2D\Common\b2Math.h>

#include "../Utils.hpp"

namespace MKI2D
{
	namespace Math
	{
		/// <summary> Wrapper class for sf::Vector2f and b2Vec2 </summary>
		template <typename T>
		class Vector2 : public sf::Vector2f, public b2Vec2
		{
		public:
			using sf::Vector2<T>::x;
			using sf::Vector2<T>::y;
			
			Vector2<T>();
			Vector2<T>(T x, T y);
			explicit Vector2<T>(sf::Vector2<T> vector2);
			explicit Vector2<T>(b2Vec2 vector2) {};

			void SetZero() { sf::Vector2<T>::x = 0; sf::Vector2f::y = 0; }
			void Set(float nx, float ny) { sf::Vector2<T>::x = T (nx); sf::Vector2f::y = T (ny); }
			
			float32 operator () (int32 i) { return float32 ((&x)[i]); }
			void operator +=(const b2Vec2& v) { x += T (v.x); y += T (v.y); };
			void operator -=(const b2Vec2& v) { x -= T(v.x); y -= T(v.y); };
			void operator *=(const b2Vec2& v) { x *= T(v.x); y *= T(v.y); };

			float32 Length() const
			{
				return float32 (sqrt(x*x + y*y));
			}

			float32 LengthSquared() const
			{
				return float32 (x*x + y*y);
			}

			float32 Normalize()
			{
				float32 length = Length();
				if (length < FLT_EPSILON)
				{
					return 0.0f;
				}
				float32 invLength = 1.0f / length;
				x *= T (invLength);
				y *= T (invLength);

				return length;
			}

			bool IsValid() const
			{
				return b2IsValid(float32(x)) && b2IsValid(float32 (y));
			}

			b2Vec2 Skew() const
			{
				return b2Vec2(float32 (-y), float32 (x));
			}

		};



		// -- Operators

		// Inspired by sf::Vector2 and b2Vec2
		template<typename T>
		Math::Vector2<T> operator -(b2Vec2 v) { v.x = -v.x; v.y = -v.y; };
		
		typedef Math::Vector2<float> Vector2f;
	}
}