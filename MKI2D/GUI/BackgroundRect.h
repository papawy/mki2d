#pragma once

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

#include "../Managers.hpp"

#include "Widget.h"

namespace MKI2D
{
	namespace GUI
	{
		class BackgroundRect : public GUI::Widget
		{
		public:
			BackgroundRect(sf::RenderWindow &window);

			BackgroundRect(sf::RenderWindow &window, sf::Texture &texture);

			BackgroundRect(sf::RenderWindow &window, sf::Texture &texture, sf::FloatRect textureCornerRect, sf::FloatRect textureSideRect, sf::FloatRect textureBckGrdRect);

			BackgroundRect(sf::RenderWindow &window, sf::Texture &texture, sf::FloatRect textureCornerRect, sf::FloatRect textureSideRect, sf::FloatRect textureBckGrdRect, sf::FloatRect pos);

			~BackgroundRect();

			//

			void copyBackgroundRectParams(BackgroundRect &copy);

			//
			void setTexture(sf::Texture &texture);

			void setWindow(sf::RenderWindow &window);

			void setPosition(sf::FloatRect pos);

			void setTextureRect(sf::FloatRect textureCorner, sf::FloatRect textureSide, sf::FloatRect textureBckGrd);

			//

			sf::Texture* getTexture();

			sf::FloatRect getSize();

			//

			bool isClicked();

			bool isMouseOn();

			//

			void update();

		protected:

			void draw(sf::RenderTarget& target, sf::RenderStates states) const;

			sf::FloatRect	m_texCorner;

			sf::FloatRect	m_texSide;

			sf::FloatRect	m_texBckGrd;

			sf::VertexArray	m_vertexArray;

			sf::Texture		*m_texture;

			sf::RenderWindow *m_window;

		};

	}
}