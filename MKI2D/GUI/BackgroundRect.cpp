#include "BackgroundRect.h"

namespace MKI2D
{
	namespace GUI
	{
		BackgroundRect::BackgroundRect(sf::RenderWindow &window) 
			: m_window(&window)
		{
			m_vertexArray.setPrimitiveType(sf::Quads);

			m_texture = NULL;
		}

		BackgroundRect::BackgroundRect(sf::RenderWindow &window, sf::Texture &texture) 
			: m_window(&window),
			m_texture(&texture)
		{
			m_vertexArray.setPrimitiveType(sf::Quads);

			m_texture = &texture;
		}

		BackgroundRect::BackgroundRect(sf::RenderWindow &window, sf::Texture &texture, sf::FloatRect textureCornerRect, sf::FloatRect textureSideRect, sf::FloatRect textureBckGrdRect) 
			: m_window(&window),
			m_texture(&texture)
		{
			m_vertexArray.setPrimitiveType(sf::Quads);

			m_texture = &texture;

			this->setTextureRect(textureCornerRect, textureSideRect, textureBckGrdRect);
		}

		BackgroundRect::BackgroundRect(sf::RenderWindow &window, sf::Texture &texture, sf::FloatRect textureCornerRect, sf::FloatRect textureSideRect, sf::FloatRect textureBckGrdRect, sf::FloatRect pos) 
			: m_window(&window),
			m_texture(&texture)
		{
			m_vertexArray.setPrimitiveType(sf::Quads);

			this->setTextureRect(textureCornerRect, textureSideRect, textureBckGrdRect);

			this->setPosition(pos);
		}

		BackgroundRect::~BackgroundRect() { }

		//

		void BackgroundRect::copyBackgroundRectParams(BackgroundRect &copy)
		{
			m_texCorner = copy.m_texCorner;

			m_texSide = copy.m_texSide;

			m_texBckGrd = copy.m_texBckGrd;

			m_vertexArray = copy.m_vertexArray;

			m_texture = copy.m_texture;

			m_window = copy.m_window;
		}

		//

		void BackgroundRect::setTexture(sf::Texture &texture)
		{
			m_texture = &texture;
		}

		void BackgroundRect::setWindow(sf::RenderWindow &window)
		{
			m_window = &window;
		}

		void BackgroundRect::setPosition(sf::FloatRect pos)
		{
			if (m_texture != NULL)
			{
				m_vertexArray.clear();

				int nbr_tile_X = (int)(pos.width / (m_texBckGrd.width - m_texBckGrd.left));
				int nbr_tile_Y = (int)(pos.height / (m_texBckGrd.height - m_texBckGrd.top));

				if (nbr_tile_X < 2)
					nbr_tile_X = 2;

				if (nbr_tile_Y < 2)
					nbr_tile_Y = 2;

				sf::Vector2f tileSize(m_texBckGrd.width - m_texBckGrd.left, m_texBckGrd.height - m_texBckGrd.top);

				m_vertexArray.resize(nbr_tile_X * nbr_tile_Y * 4);
				for (int w = 0; w < nbr_tile_X; w++)
				{
					for (int h = 0; h < nbr_tile_Y; h++)
					{
						sf::Vertex* quad = &m_vertexArray[(w + h * nbr_tile_X) * 4];

						// Corners
						if ((w == 0) && (h == 0))
						{
							quad[0].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.top);
							quad[1].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.top);
							quad[2].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.height);
							quad[3].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.height);
						}
						else if ((w == nbr_tile_X - 1) && (h == 0))
						{
							quad[0].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.top);
							quad[1].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.top);
							quad[2].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.height);
							quad[3].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.height);
						}
						else if ((w == nbr_tile_X - 1) && (h == nbr_tile_Y - 1))
						{
							quad[0].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.height);
							quad[1].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.height);
							quad[2].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.top);
							quad[3].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.top);
						}
						else if ((w == 0) && (h == nbr_tile_Y - 1))
						{
							quad[0].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.height);
							quad[1].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.height);
							quad[2].texCoords = sf::Vector2f(m_texCorner.width, m_texCorner.top);
							quad[3].texCoords = sf::Vector2f(m_texCorner.left, m_texCorner.top);
						}
						// Sides
						else if (w == 0)
						{
							quad[0].texCoords = sf::Vector2f(m_texSide.left, m_texSide.top);
							quad[1].texCoords = sf::Vector2f(m_texSide.width, m_texSide.height);
							quad[2].texCoords = sf::Vector2f(m_texSide.width, m_texSide.height);
							quad[3].texCoords = sf::Vector2f(m_texSide.left, m_texSide.top);
						}
						else if (h == 0)
						{
							quad[0].texCoords = sf::Vector2f(m_texSide.left, m_texSide.top);
							quad[1].texCoords = sf::Vector2f(m_texSide.width, m_texSide.top);
							quad[2].texCoords = sf::Vector2f(m_texSide.width, m_texSide.height);
							quad[3].texCoords = sf::Vector2f(m_texSide.left, m_texSide.height);
						}
						else if (w == nbr_tile_X - 1)
						{
							quad[0].texCoords = sf::Vector2f(m_texSide.left, m_texSide.height);
							quad[1].texCoords = sf::Vector2f(m_texSide.width, m_texSide.top);
							quad[2].texCoords = sf::Vector2f(m_texSide.width, m_texSide.top);
							quad[3].texCoords = sf::Vector2f(m_texSide.left, m_texSide.height);
						}
						else if (h == nbr_tile_Y - 1)
						{
							quad[0].texCoords = sf::Vector2f(m_texSide.left, m_texSide.height);
							quad[1].texCoords = sf::Vector2f(m_texSide.width, m_texSide.height);
							quad[2].texCoords = sf::Vector2f(m_texSide.width, m_texSide.top);
							quad[3].texCoords = sf::Vector2f(m_texSide.left, m_texSide.top);
						}
						// Background
						else
						{
							quad[0].texCoords = sf::Vector2f(m_texBckGrd.left, m_texBckGrd.top);
							quad[1].texCoords = sf::Vector2f(m_texBckGrd.width, m_texBckGrd.top);
							quad[2].texCoords = sf::Vector2f(m_texBckGrd.width, m_texBckGrd.height);
							quad[3].texCoords = sf::Vector2f(m_texBckGrd.left, m_texBckGrd.height);
						}

						quad[0].position = sf::Vector2f(pos.left + (w *  tileSize.x), pos.top + (h *  tileSize.y));
						quad[1].position = sf::Vector2f(pos.left + ((w + 1) *  tileSize.x), pos.top + (h *  tileSize.y));
						quad[2].position = sf::Vector2f(pos.left + ((w + 1) *  tileSize.x), pos.top + ((h + 1) *  tileSize.y));
						quad[3].position = sf::Vector2f(pos.left + (w *  tileSize.x), pos.top + ((h + 1) *  tileSize.y));
					}
				}
			}
		}

		void BackgroundRect::setTextureRect(sf::FloatRect textureCorner, sf::FloatRect textureSide, sf::FloatRect textureBckGrd)
		{
			m_texCorner = textureCorner;
			m_texSide = textureSide;
			m_texBckGrd = textureBckGrd;
		}

		sf::FloatRect BackgroundRect::getSize()
		{
			return m_vertexArray.getBounds();
		}

		sf::Texture* BackgroundRect::getTexture()
		{
			return m_texture;
		}

		//

		bool BackgroundRect::isMouseOn()
		{
			return sf::FloatRect(this->getPosition().x, this->getPosition().y, this->getSize().width, this->getSize().height).contains(m_window->mapPixelToCoords(sf::Mouse::getPosition(*m_window)));
		}

		bool BackgroundRect::isClicked()
		{
			if (this->isMouseOn())
			{
				return sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
			}
			else
				return false;
		}

		//

		void BackgroundRect::update()
		{

		}

		//

		void BackgroundRect::draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			states.transform *= getTransform();

			states.texture = m_texture;

			target.draw(m_vertexArray, states);
		}
	}
}