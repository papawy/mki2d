#pragma once

#include <array>

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

#include "../Managers.hpp"

#include "Widget.h"

#include "BackgroundRect.h"

namespace MKI2D
{
	namespace GUI
	{
		class Button : public Widget
		{
		public:

			Button(sf::RenderWindow &window, sf::Texture &texture);

			Button(sf::RenderWindow &window, BackgroundRect &bckgrd);

			~Button();

			// Button states enumeration
				// -> defined in Widget class

			// Accessors

			sf::FloatRect getSize();

			// Setters

			void setSize(sf::FloatRect pos);

			// State settings

			void setDisabled(bool disabled);

			// Texture settings

			void setTexture(sf::Texture &texture);

			//void setTextureRect(sf::IntRect idle, sf::IntRect mouseOn, sf::IntRect mouseClick);

			void setBackgroundRectForState(e_WidgetStates state, sf::FloatRect textureCorner, sf::FloatRect textureSide, sf::FloatRect textureBckGrd);

			void setBackgroundRectForState(e_WidgetStates state, BackgroundRect &rect);

			void setDefaultState(e_WidgetStates state);

			// Text settings

			void setFont(sf::Font &font);
			
			void setText(std::string text);

			void setTextColor(sf::Color color);

			void setTextSize(unsigned int size);

			// Events

			bool isDisabled();

			bool isClicked();

			bool isMouseOn();

			// Updating : IMPORTANT !

			void update();

		protected:

			void draw(sf::RenderTarget& target, sf::RenderStates states) const;

			bool m_isTextureUsed(e_WidgetStates state);

			bool m_disabled;

			sf::RenderWindow *m_window;


			e_WidgetStates m_actualState;

			e_WidgetStates m_defaultState;

			//BackgroundRect m_background[MAX_STATES];

			std::array<BackgroundRect, MAX_STATES> m_background;

			bool m_textureUsed[MAX_STATES];


			sf::Font* m_textFont;

			sf::Text m_text;
		};
	}
}