#include "../Utils.hpp"

#include "Utils.h"

namespace MKI2D
{
	namespace GUI
	{

		void allignWidget(AlignTypes align, GUI::Widget &widget1, GUI::Widget &widget2, float margin)
		{
			switch (align)
			{
			case MKI2D::GUI::ALIGN_VERTICALY_RIGHT:
				widget2.setPosition(sf::Vector2f(widget1.getPosition().x, widget1.getPosition().y + widget1.getSize().height + margin));
				break;
				
			case MKI2D::GUI::ALIGN_VERTICALY_CENTER:
				widget2.setPosition(sf::Vector2f(widget1.getPosition().x + (widget1.getSize().width / 2) - (widget2.getSize().width / 2), widget1.getPosition().y + widget1.getSize().height + margin));
				break;

			case MKI2D::GUI::ALIGN_VERTICALY_LEFT:
				widget2.setPosition(sf::Vector2f(widget1.getPosition().x + widget1.getSize().width - widget2.getSize().width, widget1.getPosition().y + widget1.getSize().height + margin));
				break;

			case MKI2D::GUI::ALIGN_HORIZONTALY_RIGHT:
				widget2.setPosition(sf::Vector2f(widget1.getPosition().x + widget1.getSize().width + margin , widget1.getPosition().y));
				break;

			case MKI2D::GUI::ALIGN_HORIZONTALY_LEFT:
				widget2.setPosition(sf::Vector2f(widget1.getPosition().x - widget2.getSize().width - margin, widget1.getPosition().y));
				break;

			default:
				widget2.setPosition(sf::Vector2f(widget1.getPosition().x, widget1.getPosition().y + widget1.getSize().height + margin));
				break;
			}
		}
	}
}