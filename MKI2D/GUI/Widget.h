#pragma once

#include <iostream>

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

namespace MKI2D
{
	namespace GUI
	{
		enum e_WidgetStates
		{
			IDLE,
			MOUSE_ON,
			MOUSE_CLICK,
			DISABLED,
			MAX_STATES
		};

		class Widget : public sf::Drawable, public sf::Transformable
		{
		public:

			Widget();
			virtual~Widget();

			virtual sf::FloatRect getSize() = 0;

			virtual void update() = 0;

		protected:

			virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;

		};
	}
}