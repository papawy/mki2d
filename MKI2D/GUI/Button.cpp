#include "Button.h"

namespace MKI2D
{
	namespace GUI
	{
		Button::Button(sf::RenderWindow &window, sf::Texture &texture) 
			: m_background({ { window, window, window, window } })
		{

			for (int i = 0; i < MAX_STATES; ++i)
			{
				m_background[i].setTexture(texture);
				m_background[i].setWindow(window);
			}

			m_actualState = e_WidgetStates::IDLE;

			m_defaultState = e_WidgetStates::IDLE;

			m_disabled = false;

			m_window = &window;

			m_textFont = NULL;

			m_textureUsed[0] = false;
			m_textureUsed[1] = false;
			m_textureUsed[2] = false;
			m_textureUsed[3] = false;
		}

		Button::Button(sf::RenderWindow &window, BackgroundRect &bckgrd) 
			: m_background({ { window, window, window, window } })
		{
			for (int i = 0; i < MAX_STATES; ++i)
			{
				m_background[i].copyBackgroundRectParams(bckgrd);
			}

			m_actualState = e_WidgetStates::IDLE;

			m_defaultState = e_WidgetStates::IDLE;

			m_disabled = false;

			m_window = &window;

			m_textFont = NULL;

			m_textureUsed[0] = false;
			m_textureUsed[1] = false;
			m_textureUsed[2] = false;
			m_textureUsed[3] = false;
		}

		Button::~Button()
		{

		}

		// Accessors

		sf::FloatRect Button::getSize()
		{
			return m_background[m_actualState].getSize();
		}

		// Setters

		void Button::setSize(sf::FloatRect pos)
		{
			for (int i = 0; i < MAX_STATES; ++i)
			{
				m_background[i].setPosition(pos);
			}
		}

		// State setting

		void Button::setDisabled(bool disabled)
		{
			m_disabled = disabled;
		}

		bool Button::isDisabled()
		{
			return m_disabled;
		}

		// --  Protected

		bool Button::m_isTextureUsed(e_WidgetStates state)
		{
			return m_textureUsed[state];
		}

		// Texture settings

		void Button::setTexture(sf::Texture &texture)
		{
			m_background[m_actualState].setTexture(texture);
		}

		void Button::setBackgroundRectForState(e_WidgetStates state, sf::FloatRect textureCorner, sf::FloatRect textureSide, sf::FloatRect textureBckGrd)
		{
			m_background[state].setTextureRect(textureCorner, textureSide, textureBckGrd);

			m_textureUsed[state] = true;
		}

		void Button::setBackgroundRectForState(e_WidgetStates state, BackgroundRect &rect)
		{
			m_background[state].copyBackgroundRectParams(rect);

			m_textureUsed[state] = true;
		}

		void Button::setDefaultState(e_WidgetStates state)
		{
			m_defaultState = state;
		}

		// Text settings

		void Button::setFont(sf::Font &font)
		{
			m_textFont = &font;
			m_text.setFont(*m_textFont);
		}

		void Button::setText(std::string text)
		{
			m_text.setString(text);
			m_text.setOrigin(static_cast<float>(m_text.getLocalBounds().width) / 2, static_cast<float>(m_text.getCharacterSize()) / 2);
			m_text.setPosition(this->getSize().width / 2, this->getSize().height / 2);
			while ((m_text.getGlobalBounds().width >= this->getSize().width) || (m_text.getGlobalBounds().height >= this->getSize().height))
			{
				m_text.setCharacterSize(m_text.getCharacterSize() - 1);
			}
			m_text.setOrigin(static_cast<float>(m_text.getLocalBounds().width) / 2, static_cast<float>(m_text.getCharacterSize()) / 2);
			m_text.setPosition(this->getSize().width / 2, this->getSize().height / 2);
		}

		void Button::setTextColor(sf::Color color)
		{
			m_text.setColor(color);
		}

		void Button::setTextSize(unsigned int size)
		{
			m_text.setCharacterSize(size);
			m_text.setOrigin(static_cast<float>(m_text.getLocalBounds().width) / 2, static_cast<float>(m_text.getCharacterSize()) / 2);
			m_text.setPosition(this->getSize().width / 2, this->getSize().height / 2);
		}

		// Events

		bool Button::isClicked()
		{
			if ((this->isMouseOn()) && (this->isDisabled() == false))
			{
				return sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
			}
			else
				return false;
		}

		bool Button::isMouseOn()
		{
			return sf::FloatRect(this->getPosition().x, this->getPosition().y, this->getSize().width, this->getSize().height).contains(m_window->mapPixelToCoords(sf::Mouse::getPosition(*m_window)));
		}

		// Updating

		void Button::update()
		{
			if (isMouseOn() == true)
			{
				if (isClicked() == true)
				{
					if (m_isTextureUsed(e_WidgetStates::MOUSE_CLICK) == true)
						m_actualState = e_WidgetStates::MOUSE_CLICK;
					else
						m_actualState = m_defaultState;
				}
				else
				{
					if (m_isTextureUsed(e_WidgetStates::MOUSE_ON) == true)
						m_actualState = e_WidgetStates::MOUSE_ON;
					else
						m_actualState = m_defaultState;
				}
			}
			else
			{
				if (m_isTextureUsed(e_WidgetStates::IDLE))
					m_actualState = e_WidgetStates::IDLE;
				else
					m_actualState = m_defaultState;
			}

			if (m_disabled == true)
			{
				if (m_isTextureUsed(e_WidgetStates::DISABLED))
					m_actualState = e_WidgetStates::DISABLED;
				else
					m_actualState = m_defaultState;
			}
				
		}

		// Drawing

		void Button::draw(sf::RenderTarget& target, sf::RenderStates states) const
		{
			states.transform *= getTransform();

			target.draw(m_background[m_actualState], states);

			target.draw(m_text, states);

		}
	}
}