#pragma once

#include "../Utils.hpp"

#include "Widget.h"

namespace MKI2D {
	namespace GUI
	{
		enum AlignTypes
		{
			ALIGN_VERTICALY_RIGHT,
			ALIGN_VERTICALY_CENTER,
			ALIGN_VERTICALY_LEFT,

			ALIGN_HORIZONTALY_RIGHT,
			ALIGN_HORIZONTALY_LEFT
		};

		void allignWidget(AlignTypes align, GUI::Widget &widget1, GUI::Widget &widget2, float margin = 10.0);
	}
}
