#pragma once

#include <iostream>

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

#include "../Managers.hpp"

#include "Widget.h"

namespace MKI2D
{
	namespace GUI
	{
		class CheckBox : public Widget
		{
		public:

			enum e_CheckStates {
				IDLE,
				MOUSE_ON,
				CHECKED,
				DISABLED
			};

			CheckBox();
			~CheckBox();

			// Accessors

			sf::FloatRect getSize();

			// State settings

			void setChecked(bool checked);
			void setDisabled(bool disabled);

			// Texture settings



			void update();

		protected:

			void draw(sf::RenderTarget& target, sf::RenderStates states) const;

			bool m_isTextureUsed(e_CheckStates state);

			bool m_disabled;

			sf::Window *m_window;


			e_CheckStates m_actualState;

			e_CheckStates m_defaultState;


			sf::VertexArray m_buttonStates[4];

			sf::IntRect m_buttonRect[4];

			sf::Texture *m_texture;

			bool m_textureUsed[4];


		};
	}
}