#pragma once

#include <iostream>
#include <map>

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

namespace MKI2D
{
	namespace Utils
	{
		/* deprecated better use TextureAssets */
		class TextureManager
		{
		public:
			TextureManager();
			~TextureManager();

			bool addTexture(std::string fileName, std::string textureName, bool smooth = false, sf::Color colorMask = sf::Color::Transparent);

			bool deleteTexture(std::string textureName);

			bool isTextureLoaded(std::string textureName);

			sf::Texture* getTexture(std::string textureName);

		protected:
			std::map<std::string, sf::Texture> m_textureArray;
		};

	}
}