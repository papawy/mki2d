#include "TextureManager.h"

namespace MKI2D
{
	namespace Utils
	{
		TextureManager::TextureManager()
		{
		}

		TextureManager::~TextureManager()
		{
			m_textureArray.erase(m_textureArray.begin(), m_textureArray.end());
		}

		bool TextureManager::addTexture(std::string fileName, std::string textureName, bool smooth, sf::Color colorMask)
		{
			sf::Image tmpImage;
			if (!tmpImage.loadFromFile(fileName))
			{
				return false;
			}
			else
			{
				tmpImage.createMaskFromColor(colorMask);
				if (m_textureArray[textureName].loadFromImage(tmpImage))
				{
					m_textureArray[textureName].setSmooth(smooth);
					return true;
				}
				else
					return false;

			}
		}

		bool TextureManager::deleteTexture(std::string textureName)
		{
			if (isTextureLoaded(textureName))
			{
				m_textureArray.erase(m_textureArray.find(textureName));
				return true;
			}
			else
				return false;
		}

		bool TextureManager::isTextureLoaded(std::string textureName)
		{
			if (m_textureArray.find(textureName) != m_textureArray.end())
				return true;
			else
				return false;
		}

		sf::Texture* TextureManager::getTexture(std::string textureName)
		{
			if (isTextureLoaded(textureName))
			{
				return &m_textureArray[textureName];
			}
			else
				return NULL;
		}
	}
}