#pragma once

#include <iostream>
#include <map>

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

namespace MKI2D
{
	namespace Utils
	{
		/* depcrecated better use assets*/
		class FontManager
		{
		public:
			FontManager();
			~FontManager();

			bool addFont(std::string fileName, std::string fontName);

			bool deleteFont(std::string fontName);

			bool isFontLoaded(std::string fontName);

			sf::Font* getFont(std::string fontName);

		protected:
			std::map<std::string, sf::Font> m_fontArray;

		};
	}
}