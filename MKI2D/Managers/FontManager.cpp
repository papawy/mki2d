#include "FontManager.h"

namespace MKI2D
{
	namespace Utils
	{
		FontManager::FontManager()
		{
		}

		FontManager::~FontManager()
		{
			m_fontArray.erase(m_fontArray.begin(), m_fontArray.end());
		}

		bool FontManager::addFont(std::string fileName, std::string fontName)
		{
			sf::Image tmpImage;
			if (!m_fontArray[fontName].loadFromFile(fileName))
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		bool FontManager::deleteFont(std::string fontName)
		{
			if (isFontLoaded(fontName))
			{
				m_fontArray.erase(m_fontArray.find(fontName));
				return true;
			}
			else
				return false;
		}

		bool FontManager::isFontLoaded(std::string fontName)
		{
			if (m_fontArray.find(fontName) != m_fontArray.end())
				return true;
			else
				return false;
		}

		sf::Font* FontManager::getFont(std::string fontName)
		{
			if (isFontLoaded(fontName))
			{
				return &m_fontArray[fontName];
			}
			else
				return NULL;
		}
	}
}