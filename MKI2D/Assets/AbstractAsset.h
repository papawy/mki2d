#pragma once

#include <SFML\System.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics.hpp>

#include "../Utils.hpp"

namespace MKI2D
{
	namespace Asset
	{
		enum class LOADING_STYLES
		{
			NOT_DEFINED,
			FILE,
			MEM,
			STREAM
		};

		enum class LOADING_STATES
		{
			NOT_LOADED,
			LOADED,
			STREAMING
		};

		template <typename T>
		class  AbstractAsset
		{
		public:

			AbstractAsset()
			{
				m_loadingStyle = LOADING_STYLES::NOT_DEFINED;
				m_loadingStates = LOADING_STATES::NOT_LOADED;
			}

			AbstractAsset(LOADING_STYLES loadingStyle)
			{
				m_loadingStyle = loadingStyle;
				m_loadingStates = LOADING_STATES::NOT_LOADED;
			}

			// --

			~AbstractAsset() { delete m_asset; };

			// --

			void setLoadingStyle(LOADING_STYLES loadingStyle)
			{
				m_loadingStyle = loadingStyle;
			}

			LOADING_STYLES getLoadStyle()
			{
				return m_loadingStyle;
			}

			// --

			LOADING_STATES getLoadingState()
			{
				return m_loadingStates;
			}

			// --

			virtual T& GetAsset() { return *m_asset;  };

			// --

			virtual bool Load(const std::string fileName) = 0;

			// --

			virtual void Unload() = 0;

			// --- operators

			AbstractAsset<T>& operator=(AbstractAsset<T> assignTo)
			{
				assignTo.m_loadingStyle = m_loadingStyle;
				assignTo.m_loadingStates = m_loadingStates;

				assignTo.m_asset = m_asset;
			}

		protected:
			LOADING_STYLES m_loadingStyle;
			LOADING_STATES m_loadingStates;

			T* m_asset;
		};
	}
}