#pragma once

#include "AbstractAsset.h"

namespace MKI2D
{
	namespace Asset
	{
		class FontAsset : public AbstractAsset<sf::Font>
		{
		public:
			FontAsset() : AbstractAsset() {};
			FontAsset(std::string fileName) : AbstractAsset(LOADING_STYLES::FILE)
			{
				if (!this->Load(fileName))
					throw std::invalid_argument("ERROR : Cannot load texture asset form file !");
				else
					m_loadingStates = LOADING_STATES::LOADED;
			}

			// --

			~FontAsset() { delete m_asset; };

			// --

			bool Load(const std::string fileName);

			// -- 

			void Unload()  { delete m_asset; m_loadingStates = LOADING_STATES::NOT_LOADED; };
		};
	}
}