#pragma once

#include "AbstractAsset.h"

namespace MKI2D
{
	namespace Asset
	{
		class TextureAsset : public AbstractAsset<sf::Texture>
		{
		public:
			TextureAsset() : AbstractAsset() {};
			TextureAsset(std::string fileName) : AbstractAsset(LOADING_STYLES::FILE){
				if (!this->Load(fileName))
					throw std::invalid_argument("ERROR : Cannot load texture asset form file !");
				else
					m_loadingStates = LOADING_STATES::LOADED;
			};

			// --

			~TextureAsset() { delete m_asset; };

			// --

			bool Load(const std::string fileName);
			bool Load(const std::string fileName, const sf::IntRect& subArea);

			bool Load(const sf::Image& image);
			bool Load(const sf::Image& image, const sf::IntRect& subArea);

			// --

			void Unload()  { delete m_asset; m_loadingStates = LOADING_STATES::NOT_LOADED; };
		};
	}
}