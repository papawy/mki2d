#include <stdexcept>

#include "TextureAsset.h"

namespace MKI2D
{
	namespace Asset
	{
		bool TextureAsset::Load(const std::string fileName, const sf::IntRect& subArea)
		{
			m_asset = new sf::Texture;
			if (m_asset->loadFromFile(fileName, subArea))
			{
				m_loadingStates = LOADING_STATES::LOADED;
				m_loadingStyle = LOADING_STYLES::FILE;
				return true;
			}
			else
			{
				m_loadingStates = LOADING_STATES::NOT_LOADED;
				delete m_asset;
				return false;
			}
		}

		bool TextureAsset::Load(const std::string fileName)
		{
			return Load(fileName, sf::IntRect(0, 0, 0, 0));
		}

		bool TextureAsset::Load(const sf::Image& image, const sf::IntRect& subArea)
		{
			m_asset = new sf::Texture;
			if (m_asset->loadFromImage(image, subArea))
			{
				m_loadingStates = LOADING_STATES::LOADED;
				m_loadingStyle = LOADING_STYLES::FILE;
				return true;
			}
			else
			{
				m_loadingStates = LOADING_STATES::NOT_LOADED;
				delete m_asset;
				return false;
			}
		}

		bool TextureAsset::Load(const sf::Image& image)
		{
			return Load(image, sf::IntRect(0, 0, 0, 0));
		}
	}
}