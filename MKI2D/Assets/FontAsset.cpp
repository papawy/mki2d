#include "FontAsset.h"

namespace MKI2D
{
	namespace Asset
	{
		bool FontAsset::Load(const std::string fileName)
		{
			m_asset = new sf::Font;
			if (m_asset->loadFromFile(fileName))
			{
				m_loadingStates = LOADING_STATES::LOADED;
				m_loadingStyle = LOADING_STYLES::FILE;
				return true;
			}
			else
			{
				m_loadingStates = LOADING_STATES::NOT_LOADED;
				delete m_asset;
				return false;
			}
		}
	}
}